package sqli.df.tp.tp_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InitTestSteps {
	private Bar bar;
	private Person pignon;
	private Person leblanc;
	private Map<String, Person> listPerson;
	private int drinkAmount;
	
	private Person anotherPerson;

	 //Background
	@Given("^there is a bar named \"([^\"]*)\"$")
	public void there_is_a_bar_named(String arg1) throws Throwable {
		bar =  new Bar(arg1);
	}

	@Given("^there is only (\\d+) seats$")
	public void there_is_only_seats(int arg1) throws Throwable {
		bar.setMaxSeats(arg1);
	}

	@Given("^Mr\\.Pignon and Mr\\.LeBlanc are going to a bar$")
	public void mr_Pignon_and_Mr_LeBlanc_are_going_to_a_bar() throws Throwable {
		pignon = new Person();
		leblanc = new Person();
		listPerson = new HashMap<>();
		listPerson.put("Pignon",pignon);
		listPerson.put("LeBlanc",leblanc);
	}

	//First step of each scenarii
	@Given("^there is already (\\d+) persons$")
	public void there_is_already_persons(int arg1) throws Throwable {
		bar.setNumberOfPerson(arg1);
	}

	@When("^they arrive at the bar$")
	public void they_arrive_at_the_bar() throws Throwable {
		bar.addPerson(listPerson.values());
	}

	@Then("^they are being denied$")
	public void they_are_being_denied() throws Throwable {
		assertFalse(pignon.isAccepted() && leblanc.isAccepted());
	}

	@Then("^they are being accepted$")
	public void they_are_being_accepted() throws Throwable {
		assertTrue(pignon.isAccepted() && leblanc.isAccepted());
	}

	@Then("^the bar is full$")
	public void the_bar_is_full() throws Throwable {
		assertTrue(bar.isFull());
	}

	//step last person is denied
	@When("^another person arrived at the bar$")
	public void another_person_arrived_at_the_bar() throws Throwable {
		anotherPerson = new Person();
		bar.addPerson(Arrays.asList(anotherPerson));
	}

	@Then("^the last person is denied$")
	public void the_last_person_is_denied() throws Throwable {
		assertFalse(anotherPerson.isAccepted());
	}


	//step given : order drink and have to pay
	@Given("^They order one drink each at (\\d+)€$")
	public void they_order_one_drink_each_at_amount(int amount) throws Throwable {
		drinkAmount = amount;
	}

	@Given("^Pignon have drink (\\d+) Cocktail$")
	public void pignon_have_drink_n_Cocktail(int numberOfCocktail) throws Throwable {
		pignon.setNumberOfDrink(numberOfCocktail);
	}

	@Given("^(.*) will pay (\\d+) drink$")
	public void a_person_will_pays_n_drink(String name, int numberOfDrink) throws Throwable {
		Person person = listPerson.get(name);
		person.setDrinkAmount(numberOfDrink*drinkAmount);
	}

	@Given("^They have to pay$")
	public void they_have_to_pay() throws Throwable {
		this.bar.createBill(leblanc,leblanc.getDrinkAmount());
		this.bar.createBill(pignon,pignon.getDrinkAmount());
	}

	//step they verify and pay the bill
	@Then("^The bill is correct$")
	public void the_bill_is_correct() throws Throwable {
		assertEquals(leblanc.getDrinkAmount(), bar.getBill(leblanc).getAmount());
		assertEquals(pignon.getDrinkAmount(), bar.getBill(pignon).getAmount());
	}

	@Then("^(.*) pays (\\d+) drink$")
	public void a_person_pays_n_drink(String name, int numberOfDrink) throws Throwable {
		Person person = listPerson.get(name);
		this.bar.getBill(person).paidTheBill(person);
		assertTrue(bar.getBill(person).isPayed());
		assertEquals(person,bar.getBill(person).getPayer());
	}


	//step verify if MrPignon is happy or not
	@Then("^Pignon should be happy : happy$")
	public void pignon_should_be_happy_happy() throws Throwable {
		assertTrue(pignon.isHappy());
	}

	@Then("^Pignon should be happy : unhappy$")
	public void pignon_should_be_happy_unhappy() throws Throwable {
		assertFalse(pignon.isHappy());
	}
}
