package main.java.com.github.jmaillard.testsautomatises.calculator.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class CalculatorTest {
	
	Calculator c = new Calculator();
	
	@Test
	public void additionPositiveInteger() {
		
		//GIVEN
		int a = 2, b = 3;
		
		//WHEN
		int res = c.sum(a, b);
		
		//THEN
		Assert.assertEquals(5, res);
	}
	

	@Test
	public void additionRelativeInteger() {
		
		//GIVEN
		int a = 2, b = -3;
				
		//WHEN
		int res = c.sum(a, b);
				
		//THEN
		Assert.assertEquals(-1, res);
	}
	
	@Test
	public void minusPositiveInteger() {
		
		//GIVEN
		int a = 2, b = 3;
		
		//WHEN
		int res = c.minus(a, b);
		
		//THEN
		Assert.assertEquals(-1, res);
	}
	
	@Test
	public void minusRelativeInteger() {
		
		//GIVEN
		int a = 2, b = -3;
		
		//WHEN
		int res = c.minus(a, b);
		
		//THEN
		Assert.assertEquals(5, res);
	}
	
	@Test
	public void divivePositiveInteger() {
		
		//GIVEN
		int a = 10, b = 2;
		
		//WHEN
		int res = c.divide(a, b);
		
		//THEN
		Assert.assertEquals(5, res);
	}
	
	@Test(expected = ArithmeticException.class)
	public void divideByZeroError() {
		//GIVEN
		int a = 10, b = 0;
		
		//WHEN
		int res = c.divide(a, b);
		
		//THEN
		Assert.assertEquals(0, res);
	}
	
	@Test
	public void multiplyPositiveInteger() {
		//GIVEN
		int a = 2, b = 3;
		
		//WHEN
		int res = c.multiply(a, b);
		
		//THEN
		Assert.assertEquals(6, res);
	}

	@Test
	public void multiplyByZero() {
		
		//GIVEN
		int a = 2, b = 0;
		
		//WHEN
		int res = c.multiply(a, b);
		
		//THEN
		Assert.assertEquals(0, res);

	}
	
}
