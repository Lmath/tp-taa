package sqli.df.tp.tp_test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Bar {

	private int maxSeats;
	private String name;
	private int numberOfPerson;
	private boolean isFull;
	private Map<Person,Bill> bills;

	public Bar(String arg1) {
		this.name = arg1;
		this.isFull = false;
		this.bills = new HashMap<>();
	}

	public void setMaxSeats(int arg1) {
		this.maxSeats = arg1;
	}

	public void setNumberOfPerson(int arg1) {
		this.numberOfPerson = arg1;

	}

	public void addPerson(Collection<Person> listPerson) {
		if((listPerson.size() + numberOfPerson) <= maxSeats) {
			this.numberOfPerson += listPerson.size();
			listPerson.forEach(p -> p.setAccepted(true));
		} else {
			listPerson.forEach(p -> p.setAccepted(false));
			this.isFull = true; 
		}
	}

	public boolean isFull() {
		return this.isFull;
	}

	public void createBill(Person person, int amount){
		this.bills.put(person, new Bill(amount));
	}

	public Bill getBill(Person person) {
		return bills.get(person);
	}
}
