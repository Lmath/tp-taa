package sqli.df.tp.tp_test;

public class Person {

    private int drinkAmount = 0;
    private int numberOfDrink = 0;
    private boolean accepted = false;


    public boolean isAccepted() {
        return this.accepted;
    }

    public void setAccepted(boolean b) {
        this.accepted = b;
    }

    public int getDrinkAmount() {
        return drinkAmount;
    }

    public void setDrinkAmount(int drinkAmount){
        this.drinkAmount = drinkAmount;
    }

    public void setNumberOfDrink(int numberOfDrink) {
        this.numberOfDrink = numberOfDrink;
    }

    public boolean isHappy() {
        return (this.numberOfDrink <= 1);
    }
}
